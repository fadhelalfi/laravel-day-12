<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends HomeController
{
    public function register()
    {
        return view('page.register');
    }
    public function signup(Request $request)
    {
        $firstname = $request->firstname;
        $lastname = $request->lastname;

        return view('page.home', compact('firstname', 'lastname'));
    }
}
